global.__DEV__ = true;
global.__PROD__ = false;

export  default {
  name: 'Super app',
  port: 3000,
  db: {
    url: 'mongodb://publicdb.mgbeta.ru/rkalimulin_skb4',
  },
  jwt: {
    secret: 'YOUR_SECRET'
  }
}
